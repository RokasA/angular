import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../user.service';
import {Person} from '../models/person';
import {PersonListComponent} from '../person-list/person-list.component';

@Component({
  selector: 'app-person-form-component',
  templateUrl: './person-form-component.component.html',
  styleUrls: ['./person-form-component.component.css']
})
export class PersonFormComponentComponent implements OnInit {

  personForm: FormGroup;
  newPerson: Person;
  personList: PersonListComponent;

  constructor(private formBuilder: FormBuilder, private userService: UserService) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.personForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      pid: ['', [Validators.required, Validators.min(1000000), Validators.max(9999999)]],
      surname: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      email: ['', Validators.email],
      middleName: [''],
      phone: [''],
      }

    );
  }

  submitForm() {
    console.log('submitted form');
    // console.log(this.personForm.get('pid').value);
    this.newPerson = {
      pid: this.personForm.get('pid').value,
      name: this.personForm.get('name').value,
      surname: this.personForm.get('surname').value,
      email: this.personForm.get('email').value,
    };
    console.log(this.newPerson);
    this.addPerson(this.newPerson);

  }

  addPerson(newPerson) {
    this.userService.addPerson(newPerson).subscribe(
      person => {
        console.log(person);
      },
      error => {
        console.log('error');
      },
      () => {
        console.log('completed');
      });
  }

}
