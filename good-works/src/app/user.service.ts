import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Person} from './models/person';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }


  getPersons(): Observable<Person[]> {
    return this.http.get<Person[]>('https://swedbank-demo.herokuapp.com/api/persons');
  }

  addPerson(person: Person): Observable<Person> {
    console.log(person);
    return this.http.post<Person>('https://swedbank-demo.herokuapp.com/api/persons', person);
  }

  removePerson(id: number): Observable<{}> {
    const url = 'https://swedbank-demo.herokuapp.com/api/persons/' + id;
    console.log(url);
    return this.http.delete(url);
  }



/*  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }*/
}
