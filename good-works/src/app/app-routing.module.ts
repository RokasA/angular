import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AboutComponent} from './about/about.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {PersonListComponent} from './person-list/person-list.component';
import {PersonFormComponentComponent} from "./person-form-component/person-form-component.component";

const routes: Routes = [
  { path: 'about', component: AboutComponent},
  { path: '', redirectTo: '/about', pathMatch: 'full'},
  { path: 'participants', component: PersonListComponent},
  { path: 'participants/new', component: PersonFormComponentComponent},
  { path: '**', component: PageNotFoundComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
