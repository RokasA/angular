import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {Person} from '../models/person';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
export class PersonListComponent implements OnInit {

  personsList: Person[] = [];
  // newPerson: Person = {pid: 12345, name: 'Pepega', surname: 'clap', email: 'RandomEmail@email.com'};

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.getPersons();
  }

  getPersons() {
    this.userService.getPersons().subscribe(
      persons => {
        console.log('success');
        this.personsList = persons;
      },
      error => {
        console.log('error');
      },
      () => {
        console.log('completed');
      }
    );
  }

  removePerson(id) {
    this.userService.removePerson(id).subscribe(
        person => {
          console.log('deleted');
        },
      error => {
          console.log('something went wrong with deletion');
      },
      () => {
          console.log('deletion action is complete');
          this.getPersons();
      }
    );
  }

}
