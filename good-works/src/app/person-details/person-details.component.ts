import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person} from '../models/person';
@Component({
  selector: 'app-person-details, [app-person-details]',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.css']
})
export class PersonDetailsComponent implements OnInit {
  @Input() person: Person;
  @Output() click = new EventEmitter();
  constructor() { }
  ngOnInit() {
  }

  removePerson() {
    this.click.emit(this.person.pid);
  }

}
