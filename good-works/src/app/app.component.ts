import {Component} from '@angular/core';
import {Person} from './models/person';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Fancy Web';
  isButtonPressed = false;
}



